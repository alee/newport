/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "newport-internal.h"
#include <canterbury/canterbury.h>

static void
method_request_free (MethodRequest *request_data);

/* FIXME: Download path validation rules might change based on the task
 * https://phabricator.apertis.org/T306, but as of now below rule is used.
 * This method performs download path validation sent by application/service
 * This is based on CbyProcessType, if it is of type "platform", the download path should be
 * "/home/user/Downloads/SERVICE", if it is built-in or store applications,download path should be under "/var/Applications/bundle_id/users/user_id/downloads"
 **/
static gboolean
newport_service_is_download_path_valid (CbyProcessInfo *process_info,
                                        const gchar *download_path)
{
  CbyProcessType process_type;
  gboolean is_valid_path = TRUE;
  g_autoptr (GFile) dnl_file_obj = NULL;

  process_type = cby_process_info_get_process_type (process_info);
  dnl_file_obj = g_file_new_for_path (download_path);

  if (process_type == CBY_PROCESS_TYPE_UNKNOWN)
    {
      is_valid_path = FALSE;
    }
  /* Platform services validation */
  else if (process_type == CBY_PROCESS_TYPE_PLATFORM)
    {
      g_autoptr (GFile) services_file_obj = NULL;
      g_autoptr (GFile) usrlib_file_obj = NULL;
      const gchar *apparmor_label;
      g_autoptr (GFile) apparmor_file_obj = NULL;
      g_autofree gchar *app_folder_name = NULL;
      g_autofree gchar *valid_path = NULL;
      g_autoptr (GFile) valid_path_file_obj = NULL;

      apparmor_label = cby_process_info_get_apparmor_label (process_info);
      services_file_obj = g_file_new_for_path ("/usr/bin");
      usrlib_file_obj = g_file_new_for_path ("/usr/lib");
      apparmor_file_obj = g_file_new_for_path (apparmor_label);

      if (g_file_has_prefix (apparmor_file_obj, services_file_obj) ||
          g_file_has_prefix (apparmor_file_obj, usrlib_file_obj))
        {
          app_folder_name = g_file_get_basename (apparmor_file_obj);
          valid_path = g_build_filename (
              g_get_user_special_dir (G_USER_DIRECTORY_DOWNLOAD),
              app_folder_name, NULL);
          valid_path_file_obj = g_file_new_for_path (valid_path);
        }
      else
        {
          is_valid_path = FALSE;
        }
      if (valid_path_file_obj
          && (!g_file_has_prefix (dnl_file_obj, valid_path_file_obj)))
        {
          is_valid_path = FALSE;
        }
    }
  /* built-in or store apps validation */
  else
    {
      const gchar *persistence_path;
      g_autoptr (GFile) persistence_obj = NULL;

      persistence_path = cby_process_info_get_persistence_path (process_info);
      persistence_obj = g_file_new_for_path (persistence_path);

      if (!g_file_has_prefix (dnl_file_obj, persistence_obj))
        is_valid_path = FALSE;
    }
  return is_valid_path;
}

static gboolean newport_download_is_owned_by_apparmor_context (NewportDownload *download, const gchar *apparmor_context, GError **error)
{
  const gchar *app_name=newport_download_get_downloading_app(download);
if(g_strcmp0(app_name,apparmor_context))
  {
    g_set_error (error, NEWPORT_DOWNLOAD_MANAGER_ERROR,
                 NEWPORT_DOWNLOAD_MANAGER_ERROR_FAILED, "%s",
                 "Not A Valid App to perform operation");
    return FALSE;
  }
  else
    return TRUE;
}

static gboolean
newport_download_is_valid_for_operation (MethodRequest *request_data,
                                         GAsyncResult *res,
                                         GError **error)
{
  g_autoptr (CbyProcessInfo) process_info = NULL;
  const gchar *apparmor_label = NULL;

  process_info = cby_process_info_new_for_dbus_invocation_finish (res, error);
  if (!process_info)
    return FALSE;

  apparmor_label = cby_process_info_get_apparmor_label (process_info);
  if (!newport_download_is_owned_by_apparmor_context (request_data->download,
                                                      apparmor_label, error))
    return FALSE;

  return TRUE;
}


static void
method_request_free (MethodRequest *request_data)
{
  g_return_if_fail (request_data != NULL);
  g_free (request_data->url);
  g_free (request_data->download_path);
  g_object_unref (request_data->invocation);
  if(request_data->download)
  g_object_unref (request_data->download);
  g_free (request_data);
}

void send_progress_update_to_client(NewportDownload *download,DownloadUrlInfo* pUrlInfo)
{
    newport_download_emit_download_progress(download,
            pUrlInfo->progress.u64CurrentSize,
            pUrlInfo->progress.u64TotalSize,
            pUrlInfo->progress.u64DownloadSpeed,
            pUrlInfo->progress.u64ElapsedTime,
            pUrlInfo->progress.u64RemainingTime);
}

/* Returns: (transfer full): the existing or newly created object */
GDBusObject *newport_download_url_info_ensure_dbus_object(
        DownloadUrlInfo *pUrlInfo)
{
  GDBusObject *dbus_object = NULL;

  g_return_val_if_fail (pUrlInfo != NULL, NULL);
        if (pUrlInfo->object_path)
            dbus_object = g_dbus_object_manager_get_object(
                    (GDBusObjectManager *) pDnlMgrObj.object_manager,
                    pUrlInfo->object_path);
        if (dbus_object)
        {
            return dbus_object;

        }
        else
        {
            GDBusObjectSkeleton *object;
            NewportDownload *download = newport_download_skeleton_new();
            g_signal_connect(download, "handle-pause",
                    G_CALLBACK(handle_pause_download), NULL);
            g_signal_connect(download, "handle-cancel",
                    G_CALLBACK(handle_cancel_download), NULL);
            g_signal_connect(download, "handle-resume",
                    G_CALLBACK(handle_resume_download), NULL);

            newport_download_set_downloading_app (download, pUrlInfo->pAppName);
            newport_download_set_url (download, pUrlInfo->pUrl);
            newport_download_set_state (download,
                    pUrlInfo->u32CurrentDownloadState);
            newport_download_set_downloaded_size (download,
                    pUrlInfo->progress.u64CurrentSize);
            newport_download_set_total_size (download,
                    pUrlInfo->progress.u64TotalSize);
            newport_download_set_error (download, -1);
            newport_download_set_path (download, pUrlInfo->pDownloadPath);
            object =
              g_dbus_object_skeleton_new ("/org/apertis/Newport/Download/Data");
            g_dbus_object_skeleton_add_interface (
                    object, (GDBusInterfaceSkeleton *) download);
            g_dbus_object_manager_server_export_uniquely (
                    pDnlMgrObj.object_manager, object);

            pUrlInfo->object_path = g_strdup(
                    g_dbus_object_get_object_path((GDBusObject*) object));
            g_object_unref(download);
            return (GDBusObject*) object;
        }

}

static void
newport_security_context_to_get_download_for_url_clb (GObject *source_object,
                                                   GAsyncResult *res,
                                                   gpointer user_data)
{
  GError *error=NULL;
  MethodRequest *request_data = user_data;
  g_autoptr (CbyProcessInfo) process_info = NULL;
  DownloadUrlInfo *pUrlInfo;
  const gchar *apparmor_label = NULL;

  process_info = cby_process_info_new_for_dbus_invocation_finish (res, &error);
  if (error)
    {
      g_dbus_method_invocation_return_dbus_error (
          request_data->invocation,
          "org.apertis.Newport.Service.Error.NotValidApp",
          "Could not determine credentials");
      method_request_free (request_data);
      g_error_free (error);
      return;
    }

  if (cby_process_info_get_process_type (process_info) == CBY_PROCESS_TYPE_UNKNOWN)
    {
      g_dbus_method_invocation_return_dbus_error (
          request_data->invocation,
          "org.apertis.Newport.Service.Error.AccessDenied",
          "Could not determine credentials");
      method_request_free (request_data);
      return;
    }

  apparmor_label = cby_process_info_get_apparmor_label (process_info);
  pUrlInfo = check_for_valid_url (request_data->url, apparmor_label);
  if (pUrlInfo)
    {
      newport_service_complete_get_download_for_url (
          NULL, request_data->invocation,
          (const gchar *) pUrlInfo->object_path);
    }
  else
    {
      g_dbus_method_invocation_return_dbus_error (
          request_data->invocation, "org.apertis.Newport.Service.NoDownload",
          "No Download Available");

    }

  method_request_free (request_data);
}

gboolean handle_get_download_for_url(NewportService *object,
        GDBusMethodInvocation *invocation, const gchar *pUrl,gpointer user_data)
{
  MethodRequest *request_data = g_new0 (MethodRequest, 1);
  request_data->url = g_strdup (pUrl);
  request_data->download = NULL;
  request_data->invocation = g_object_ref (invocation);
  request_data->download_path = NULL;

  cby_process_info_new_for_dbus_invocation_async (request_data->invocation, NULL,
                                                  (GAsyncReadyCallback) newport_security_context_to_get_download_for_url_clb,
                                                  request_data);

  return TRUE;
}

static void
newport_security_context_to_start_download_clb (GObject *source_object,
                                                GAsyncResult *res,
                                                gpointer user_data)
{
  GError *error=NULL;
  GError *err = NULL;
  GFile *download_file_obj=NULL;
  GFile *file_parent_obj=NULL;
  GDBusObject *dbus_object = NULL;
  NewportDownload *download = NULL;
  MethodRequest *request_data = user_data;
  DownloadUrlInfo *pUrlInfo = NULL;
  g_autoptr (CbyProcessInfo) process_info = NULL;
  const gchar *apparmor_label;

  process_info = cby_process_info_new_for_dbus_invocation_finish (res, &error);
  if (error)
    {
      g_dbus_method_invocation_return_dbus_error (
          request_data->invocation,
          "org.apertis.Newport.Service.Error.NotValidApp",
          "Could not determine credentials");
      method_request_free (request_data);
      g_error_free (error);
      return;
    }

  apparmor_label = cby_process_info_get_apparmor_label (process_info);
  if (!newport_service_is_download_path_valid (process_info,
                                               request_data->download_path))
    {
      g_dbus_method_invocation_return_dbus_error (
          request_data->invocation,
          "org.apertis.Newport.Service.Error.NotValidPath",
          "Not A valid path to download");
      method_request_free (request_data);
      return;
    }
/*  Newport always creates download directory if it does not exists.*/
  download_file_obj = g_file_new_for_path (request_data->download_path);
  file_parent_obj = g_file_get_parent (download_file_obj);
  g_file_make_directory_with_parents (file_parent_obj, NULL, &err);
  g_clear_object (&download_file_obj);
  g_clear_object (&file_parent_obj);
  if (err)
    {
      if (!g_error_matches (err, G_IO_ERROR, G_IO_ERROR_EXISTS))
        {
          g_dbus_method_invocation_return_dbus_error (
              request_data->invocation,
              "org.apertis.Newport.Service.Error.NotValidPath",
              "Can't create directory to download");
          g_error_free (err);
          method_request_free (request_data);
          return;
        }
      g_error_free (err);
    }
  pUrlInfo = look_for_url_in_db (request_data->url, apparmor_label);
  if (pUrlInfo)
    {
          dbus_object =
          newport_download_url_info_ensure_dbus_object (pUrlInfo);
          download =
          (NewportDownload *) g_dbus_object_get_interface (
              dbus_object, "org.apertis.Newport.Download");
      g_free (pUrlInfo->pDownloadFileName);
      pUrlInfo->pDownloadFileName = g_path_get_basename (
          request_data->download_path);
      NEWPORT_DEBUG("Download file name : %s", pUrlInfo->pDownloadFileName);
      g_free (pUrlInfo->pDownloadPath);
      pUrlInfo->pDownloadPath = g_strdup (request_data->download_path);
      newport_download_set_path (download, pUrlInfo->pDownloadPath);
      if (!store_url_data_in_db (pUrlInfo, &error))
        {
          g_dbus_method_invocation_return_dbus_error (
              request_data->invocation,
              "org.apertis.Newport.Service.Error.DatabaseError",
              error->message);
          goto cleanup_and_fail;
        }
      if (!pDnlMgrObj.is_preferred_network)
        {
          pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_PAUSED_BY_SYSTEM;
          newport_download_set_state (download,
                                      pUrlInfo->u32CurrentDownloadState);
          if (!store_url_data_in_db (pUrlInfo, &error))
             {
              g_dbus_method_invocation_return_dbus_error (
                  request_data->invocation,
                  "org.apertis.Newport.Service.Error.DatabaseError",
                  error->message);
              goto cleanup_and_fail;
             }
        }
      else if (pUrlInfo->u32CurrentDownloadState
          != NEWPORT_DOWNLOAD_STATE_IN_PROGRESS)
        {
          resume_url_download (download, pUrlInfo,&error);
          if (error)
            {
              g_dbus_method_invocation_return_dbus_error (
                  request_data->invocation,
                  "org.apertis.Newport.Service.Error.DatabaseError",
                  error->message);
              goto cleanup_and_fail;
            }
        }
    }
  else
    {
      DownloadUrlInfo *url_info_new = g_new0 (DownloadUrlInfo, 1);

      initialize_url_info (url_info_new);
      url_info_new->pDownloadFileName =
        g_path_get_basename (request_data->download_path);
      NEWPORT_DEBUG ("Download file name %s",
                     url_info_new->pDownloadFileName);
      url_info_new->pAppName = g_strdup (apparmor_label);
      url_info_new->pDownloadPath = g_strdup (request_data->download_path);
      url_info_new->pUrl = g_strdup (request_data->url);
      dbus_object =
        newport_download_url_info_ensure_dbus_object (url_info_new);
          download =
          (NewportDownload *) g_dbus_object_get_interface (
              dbus_object, "org.apertis.Newport.Download");
      if (pDnlMgrObj.uinDownloadCount >= g_settings_get_uint (
          pDnlMgrObj.schema, "max-parallel-download"))
        {
          url_info_new->u32CurrentDownloadState =
            NEWPORT_DOWNLOAD_STATE_QUEUED;
          newport_download_set_state (download,
                                      url_info_new->u32CurrentDownloadState);
          if (!store_url_data_in_db (url_info_new, &error))
             {
              g_dbus_method_invocation_return_dbus_error (
                  request_data->invocation,
                  "org.apertis.Newport.Service.Error.DatabaseError",
                  error->message);
              goto cleanup_and_fail;
             }
        }
      else
        {
          if (!pDnlMgrObj.is_preferred_network)
            {
              url_info_new->u32CurrentDownloadState =
                NEWPORT_DOWNLOAD_STATE_PAUSED_BY_SYSTEM;
              newport_download_set_state (download,
                                          url_info_new->u32CurrentDownloadState);
              if (!store_url_data_in_db (url_info_new, &error))
                 {
                  g_dbus_method_invocation_return_dbus_error (
                      request_data->invocation,
                      "org.apertis.Newport.Service.Error.DatabaseError",
                      error->message);
                  goto cleanup_and_fail;
                 }
            }
          else
            {
              url_info_new->u32CurrentDownloadState =
                NEWPORT_DOWNLOAD_STATE_IN_PROGRESS;
              newport_download_set_state (download,
                                          url_info_new->u32CurrentDownloadState);
              if (!store_url_data_in_db (url_info_new, &error))
                 {
                  g_dbus_method_invocation_return_dbus_error (
                      request_data->invocation,
                      "org.apertis.Newport.Service.Error.DatabaseError",
                      error->message);
                  goto cleanup_and_fail;
                 }
              pDnlMgrObj.uinDownloadCount++;
              start_new_url_download (download, url_info_new);
            }
        }

      pDnlMgrObj.pClientDownloadList =
        g_list_append (pDnlMgrObj.pClientDownloadList, url_info_new);
    }
  newport_service_complete_start_download (NULL, request_data->invocation,
      g_dbus_object_get_object_path (dbus_object));

  cleanup_and_fail:
  g_clear_pointer (&error, g_error_free);
  method_request_free (request_data);
  g_object_unref (download);
  g_clear_object (&dbus_object);

}

gboolean handle_start_download(NewportService *object,
        GDBusMethodInvocation *invocation, const gchar *pUrl,
        const gchar *pDownloadFile)
{

  /* Get the App security label  of the sender*/
  MethodRequest *request_data = g_new0 (MethodRequest, 1);
  request_data->url = g_strdup (pUrl);
  request_data->download = NULL;
  request_data->invocation = g_object_ref (invocation);
  request_data->download_path = g_strdup (pDownloadFile);

  cby_process_info_new_for_dbus_invocation_async (request_data->invocation, NULL,
                                                  (GAsyncReadyCallback) newport_security_context_to_start_download_clb,
                                                  request_data);
  return TRUE;
}


static void
newport_security_context_to_cancel_download_clb (GObject *source_object,
                                                 GAsyncResult *res,
                                                 gpointer user_data)
{
  GError *error=NULL;
  MethodRequest *request_data = user_data;

  if (newport_download_is_valid_for_operation (request_data, res, &error))
    {
      const gchar *app_name = newport_download_get_downloading_app (
          request_data->download);
      DownloadUrlInfo *pUrlInfo = look_for_url_in_db (request_data->url,
                                                      app_name);
      if (pUrlInfo)
        {
          if (pUrlInfo->u32CurrentDownloadState == NEWPORT_DOWNLOAD_STATE_IN_PROGRESS)
            {
              pUrlInfo->cancel_invocation=g_object_ref(request_data->invocation);
              pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_CANCELLED;
            }
          else
            {
              if (!dnl_mgr_remove_from_pdi (pUrlInfo, &error))
                {
                  g_dbus_method_invocation_return_dbus_error (
                      request_data->invocation,
                      "org.apertis.Newport.Service.Error.DatabaseError",
                      error->message);
                  method_request_free (request_data);
                  g_clear_pointer (&error, g_error_free);
                  return;
                }
              g_dbus_object_manager_server_unexport (pDnlMgrObj.object_manager,
                                                     pUrlInfo->object_path);
              unlink_file (pUrlInfo->pTempDownloadPath);
              pDnlMgrObj.pClientDownloadList = g_list_remove (
                  pDnlMgrObj.pClientDownloadList, pUrlInfo);
              download_url_info_free (pUrlInfo);
              newport_download_complete_cancel (request_data->download,
                                                request_data->invocation);
              method_request_free (request_data);
              return;
            }

        }
      else
        {
          g_dbus_method_invocation_return_dbus_error (
              request_data->invocation, "Newport.Service.Error.NotSupported",
              "Url is not available to cancel");
          method_request_free (request_data);
          return;
        }
    }
  else
    {
      g_dbus_method_invocation_take_error (request_data->invocation, error);
      method_request_free (request_data);
    }
}

gboolean handle_cancel_download(NewportDownload *object,
        GDBusMethodInvocation *invocation, gpointer user_data)
{
  /* Get the App security label  of the sender*/
  MethodRequest *request_data = g_new0 (MethodRequest, 1);
  request_data->url = g_strdup (newport_download_get_url (object));
  request_data->download=g_object_ref (object);
  request_data->invocation = g_object_ref (invocation);
  request_data->download_path = NULL;
  cby_process_info_new_for_dbus_invocation_async (request_data->invocation, NULL,
                                                  (GAsyncReadyCallback) newport_security_context_to_cancel_download_clb,
                                                  request_data);
  return TRUE;

}

static void
newport_security_context_to_pause_download_clb (GObject *source_object,
                                                GAsyncResult *res,
                                                gpointer user_data)
{
  MethodRequest *request_data = user_data;
  GError *error = NULL;

  if (newport_download_is_valid_for_operation (request_data, res, &error))
    {
      const gchar *app_name = newport_download_get_downloading_app (
          request_data->download);
      DownloadUrlInfo *pUrlInfo = look_for_url_in_db (request_data->url,
                                                      app_name);
      if (pUrlInfo)
        {
          /* looks like application sent a request to pause download of a pUrl */
    NEWPORT_DEBUG(
              "Sending current download status to the client. Requested pUrl:%s Download state:%d currentsize:%"G_GINT64_FORMAT,
              pUrlInfo->pUrl, pUrlInfo->u32CurrentDownloadState,
              pUrlInfo->progress.u64CurrentSize);
          if (pUrlInfo->u32CurrentDownloadState == NEWPORT_DOWNLOAD_STATE_IN_PROGRESS)
            {
              pUrlInfo->pause_invocation=g_object_ref(request_data->invocation);
              pUrlInfo->u32CurrentDownloadState =
                  NEWPORT_DOWNLOAD_STATE_PAUSED_BY_USER;
            }
          else
            {
              g_dbus_method_invocation_return_dbus_error (
                  request_data->invocation,
                  "Newport.Service.Error.NotPaused",
                  "Download not in progress");
              method_request_free (request_data);
              return;
            }
        }
      else
        {
          g_dbus_method_invocation_return_dbus_error (
              request_data->invocation, "Newport.Service.Error.NotSupported",
              "Url is not available to pause");
          method_request_free (request_data);
          return;
        }
    }
  else
    {
      g_dbus_method_invocation_take_error (request_data->invocation, error);
      method_request_free (request_data);
    }
}

gboolean handle_pause_download(NewportDownload *object,
        GDBusMethodInvocation *invocation, gpointer user_data)
{
  /* Get the App security label  of the sender*/
  MethodRequest *request_data = g_new0 (MethodRequest, 1);
  request_data->url = g_strdup (newport_download_get_url (object));
  request_data->download=g_object_ref (object);
  request_data->invocation = g_object_ref (invocation);
  request_data->download_path = NULL;
  cby_process_info_new_for_dbus_invocation_async (request_data->invocation, NULL,
                                                  (GAsyncReadyCallback) newport_security_context_to_pause_download_clb,
                                                  request_data);
  return TRUE;

}

static void
newport_security_context_to_resume_download_clb (GObject *source_object,
                                                 GAsyncResult *res,
                                                 gpointer user_data)
{
  MethodRequest *request_data = user_data;
  GError *error=NULL;

  if (newport_download_is_valid_for_operation (request_data, res, &error))
    {
      const gchar *app_name = newport_download_get_downloading_app (
          request_data->download);
      DownloadUrlInfo *pUrlInfo = look_for_url_in_db (request_data->url,
                                                      app_name);
      if (pUrlInfo)
        {
          /* looks like application sent a request to resume download of a pUrl */
          NEWPORT_DEBUG("handle_resume_download %s %d", request_data->url,
                        pUrlInfo->u32CurrentDownloadState);
          if (!pDnlMgrObj.is_preferred_network)
            {
              pUrlInfo->u32CurrentDownloadState =
                  NEWPORT_DOWNLOAD_STATE_PAUSED_BY_SYSTEM;
              newport_download_set_state (request_data->download,
                                          pUrlInfo->u32CurrentDownloadState);
              if (!store_url_data_in_db (pUrlInfo, &error))
                 {
                  g_dbus_method_invocation_return_dbus_error (
                      request_data->invocation,
                      "org.apertis.Newport.Service.Error.DatabaseError",
                      error->message);
                  method_request_free (request_data);
                  g_clear_pointer (&error, g_error_free);
                  return;
                 }
            }
          else if (pUrlInfo->u32CurrentDownloadState != NEWPORT_DOWNLOAD_STATE_IN_PROGRESS)
            {
              resume_url_download (request_data->download, pUrlInfo,&error);
              if (error)
                {
                  g_dbus_method_invocation_return_dbus_error (
                      request_data->invocation,
                      "org.apertis.Newport.Service.Error.DatabaseError",
                      error->message);
                  method_request_free (request_data);
                  g_clear_pointer (&error, g_error_free);
                  return;
                }
            }

          newport_download_emit_download_progress (
              request_data->download, pUrlInfo->progress.u64CurrentSize,
              pUrlInfo->progress.u64TotalSize,
              pUrlInfo->progress.u64DownloadSpeed,
              pUrlInfo->progress.u64ElapsedTime,
              pUrlInfo->progress.u64RemainingTime);
        }
      else
        {
          g_dbus_method_invocation_return_dbus_error (
              request_data->invocation, "Newport.Service.Error.NotSupported",
              "Url is not available to resume");
          method_request_free (request_data);
          return;
        }
      newport_download_complete_resume (request_data->download,
                                        request_data->invocation);
      method_request_free (request_data);
      return;
    }
  else
    {
      g_dbus_method_invocation_take_error (request_data->invocation, error);
      method_request_free (request_data);
    }
}

gboolean handle_resume_download(NewportDownload *object,
        GDBusMethodInvocation *invocation, gpointer user_data)
{


  /* Get the App security label  of the sender*/
  MethodRequest *request_data = g_new0 (MethodRequest, 1);
  request_data->url = g_strdup (newport_download_get_url (object));
  request_data->download=g_object_ref (object);
  request_data->invocation = g_object_ref (invocation);
  request_data->download_path = NULL;
  cby_process_info_new_for_dbus_invocation_async (request_data->invocation, NULL,
                                                  (GAsyncReadyCallback) newport_security_context_to_resume_download_clb,
                                                  request_data);
  return TRUE;
}


static void
newport_security_context_to_get_downloads_clb (GObject *source_object,
                                            GAsyncResult *res,
                                            gpointer user_data)
{
  GError *error=NULL;
  MethodRequest *request_data = user_data;
  gchar **obj_paths;
  g_autoptr (CbyProcessInfo) process_info = NULL;
  const gchar *apparmor_label = NULL;

  process_info = cby_process_info_new_for_dbus_invocation_finish (res, &error);
  if (!process_info)
    {
      g_dbus_method_invocation_take_error (request_data->invocation, error);
      method_request_free (request_data);
      return;
    }

  apparmor_label = cby_process_info_get_apparmor_label (process_info);

  obj_paths = dnl_mgr_get_download_object_paths_from_db (apparmor_label);
    if (obj_paths == NULL)
      {
        g_dbus_method_invocation_return_dbus_error (
            request_data->invocation, "org.apertis.Newport.Service.NoDownloads",
            "No Downloads Available");
        method_request_free (request_data);
        return ;
      }
    newport_service_complete_get_downloads (NULL, request_data->invocation,
                                            (const gchar * const *) obj_paths);
    g_strfreev(obj_paths);
    method_request_free (request_data);

}

gboolean handle_get_downloads(NewportService *object,
        GDBusMethodInvocation *invocation, gpointer user_data)
{

  MethodRequest *request_data = g_new0 (MethodRequest, 1);
  request_data->url = NULL;
  request_data->download = NULL;
  request_data->invocation = g_object_ref (invocation);
  request_data->download_path = NULL;
  cby_process_info_new_for_dbus_invocation_async (request_data->invocation, NULL,
                                                  (GAsyncReadyCallback) newport_security_context_to_get_downloads_clb,
                                                  request_data);

  return TRUE;
}
