/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <glib/gstdio.h>
#include <stdlib.h>
#include <glib-unix.h>
#include <string.h>
#include <unistd.h>
#include <curl/curl.h>
#include "newport.h"

#define NEWPORT_DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define NEWPORT_CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define NEWPORT_WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)

typedef struct _DownloadMgr DownloadMgr;
typedef struct _DownloadUrlInfo DownloadUrlInfo;
typedef struct _DownloadProgressData ProgressData;
typedef struct _MethodRequest MethodRequest;

struct _DownloadProgressData
{
	gdouble pProgressPercent;
	guint8 u8ProgressCount;
	guint64 u64TotalSize;
	guint64 u64CurrentSize;
	guint64 u64DownloadSpeed;
	guint64 u64ElapsedTime;		// Elapsed	(seconds)
	guint64 u64RemainingTime;		// Left		(seconds)
};

struct _DownloadMgr
{
  GList* pClientDownloadList;
  guint uinDownloadCount;
  NewportService *pDnlMgrDbusObj;
  gulong ulHandlerId;
  GDBusObjectManagerServer *object_manager;
  GSettings* schema;
  GDBusConnection *system_dbus_connection;
  gboolean is_preferred_network;
  GKeyFile *key_file;
  GSettings *configuration_schema;
};

struct _DownloadUrlInfo
{
	gchar* pAppName;
	gchar* pUrl;
	gchar* pDownloadPath;
	gchar* object_path;
	gchar* pTempDownloadPath;
	gchar* pDownloadFileName;
	guint32 u32CurrentDownloadState;
	ProgressData progress;
	CURL* pCurlObg;
	gchar pCurlErrorString[CURL_ERROR_SIZE + 1];
	curl_off_t FileOffset;
	curl_off_t OldFileOffset;
	curl_off_t FileOffsetBeginning;
	guint8 uin8RetryCount;
	glong lCurlTime;
	gint64 uin64ElapsedTimeBeg;		// Elapsed	(seconds)
	GDBusMethodInvocation *pause_invocation;
	GDBusMethodInvocation *cancel_invocation;
};

struct _MethodRequest
{
    gchar *url;
    NewportDownload *download;
    GDBusMethodInvocation *invocation;
    gchar *download_path;
};

DownloadMgr pDnlMgrObj;

DownloadUrlInfo* check_for_valid_url(const gchar *pUrl, const gchar *pAppName);
DownloadUrlInfo* look_for_url_in_db(const gchar* pUrl, const gchar* pAppName);
void start_new_url_download(NewportDownload *Download,
		DownloadUrlInfo *pUrlInfo);
void send_progress_update_to_client(NewportDownload *Download,DownloadUrlInfo* pUrlInfo);
void start_new_download_thread(GTask *task, NewportDownload *Download,
		DownloadUrlInfo *pUrlInfo, GCancellable *cancellable);
void
resume_url_download (NewportDownload *Download, DownloadUrlInfo *pUrlInfo,
                     GError **error);
gchar *newport_build_temporary_download_path(const gchar *file_name);

void initialize_url_info(DownloadUrlInfo *pUrlInfo);
/*Database related API's     */
gboolean initialize_database(GError **error);
void download_pending_urls(void);
gboolean store_url_data_in_db(DownloadUrlInfo *pUrlInfo,GError **error);
gboolean
store_all_running_downloads_in_db (GError **error);
gboolean retrieve_contents_from_db(void);
gboolean dnl_mgr_remove_from_pdi(DownloadUrlInfo *pUrlInfo,GError **error);
gchar*
newport_download_get_group_name (DownloadUrlInfo *pUrlInfo);

void halt_all_url_dnl(void);
void download_url_info_free(DownloadUrlInfo *pUrlInfo);
gchar **dnl_mgr_get_download_object_paths_from_db(const gchar *app_name);

gint unlink_file(const gchar *pFilename);

GDBusObject *newport_download_url_info_ensure_dbus_object(
		DownloadUrlInfo *pUrlInfo);
DownloadUrlInfo *download_url_info_copy(DownloadUrlInfo *url_info);
//dbus callbacks

gboolean handle_cancel_download(NewportDownload *object,
		GDBusMethodInvocation *invocation, gpointer user_data);

gboolean handle_pause_download(NewportDownload *object,
		GDBusMethodInvocation *invocation, gpointer user_data);

gboolean handle_resume_download(NewportDownload *object,
		GDBusMethodInvocation *invocation, gpointer user_data);

gboolean handle_get_downloads(NewportService *object,
		GDBusMethodInvocation *invocation, gpointer user_data);

gboolean handle_get_download_for_url(NewportService *object,
		GDBusMethodInvocation *invocation, const gchar *pUrl,gpointer user_data);

gboolean
handle_start_download (NewportService *object,
                       GDBusMethodInvocation *invocation, const gchar *pUrl,
                       const gchar *pDownloadFile);

void
dnl_mgr_register_client_clb (GObject *source_object, GAsyncResult *res,
                             gpointer pUserData);

void
dnl_mgr_get_prop_clb (GObject *source_object, GAsyncResult *res,
                      gpointer pUserData);

void
newport_check_for_preferred_network (void);
