/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gio/gio.h>
#include "newport-errors.h"

#define ERROR_PREFIX "org.apertis.Newport.DownloadManagerError."

static const GDBusErrorEntry error_entries[] =
{
    {
        NEWPORT_DOWNLOAD_MANAGER_ERROR_FAILED,
        ERROR_PREFIX "Failed"
    },
    {
        NEWPORT_DOWNLOAD_MANAGER_ERROR_NETWORK,
        ERROR_PREFIX "Network"
    },
    {
        NEWPORT_DOWNLOAD_MANAGER_ERROR_NO_MEMORY,
        ERROR_PREFIX "NoMemory"
    },
    {
        NEWPORT_DOWNLOAD_MANAGER_ERROR_UNRECOGNIZED_FORMAT,
        ERROR_PREFIX "UnrecognizedFormat"
    },
};

G_STATIC_ASSERT (G_N_ELEMENTS (error_entries) == NEWPORT_N_DOWNLOAD_MANAGER_ERRORS);

GQuark
newport_download_manager_error_quark (void)
{
    static volatile gsize id = 0;

    g_dbus_error_register_error_domain (
        "newport-download-manager-error-quark", &id,
        error_entries, G_N_ELEMENTS (error_entries));

    return (GQuark) id;
}
